$(document).ready(function () {
  $("#btn_admin").click(function (e) {
    e.stopPropagation();
    var display = $("#profile").css(["display"]);
    if (display.display === "none") {
      document.getElementById("profile").style = "display: flex";
    } else {
      document.getElementById("profile").style = "display: none";
    }
  });
});

$(document).ready(function () {
  $("body").click(function () {
    document.getElementById("profile").style = "display: none";
    if (document.getElementById("box_option_discount"))
      document.getElementById("box_option_discount").style = "display: none";
    if (document.getElementById("box_popover_id"))
      document.getElementById("box_popover_id").style = "display: none";
  });
  $("#dots").click(function (e) {
    e.stopPropagation();
    var position = $("#profile").css(["top", "right"]);
    console.log($("#dots").position());
    document.getElementById("box_option_discount").style = "display: block";
  });
  $("#box_option_discount").click(function (e) {
    e.stopPropagation();
  });
  $("#btn_choose_file_cover").click(function (e) {
    e.stopPropagation();
    $("#coverFile").click();
  });
  $("#btn_choose_file_logo").click(function (e) {
    e.stopPropagation();
    $("#logoFile").click();
  });
  $("#radioDiscount1").click(function (e) {
    document.getElementById("box_discount1").style = "display: block";
    document.getElementById("box_discount2").style = "display: none";
  });
  $("#radioDiscount2").click(function (e) {
    document.getElementById("box_discount1").style = "display: none";
    document.getElementById("box_discount2").style = "display: flex";
  });
  $("#del_payment_partner").click(function () {
    $("#payment_partner").val(null).trigger("change");
  });
  $("#del_hotel").click(function () {
    $("#hotel_apply").val(null).trigger("change");
  });
  $("#del_service").click(function () {
    $("#service").val(null).trigger("change");
  });
  $("#datetimepicker1").datetimepicker({
    format: "DD-MM-YYYY LT",
    minDate: new Date(),
  });
  $("#datetimepicker2").datetimepicker({
    format: "DD-MM-YYYY LT",
    minDate: new Date(),
  });
  $("#datetimepicker3").datetimepicker({
    format: "DD-MM-YYYY LT",
    minDate: new Date(),
  });
  $("#datetimepicker4").datetimepicker({
    format: "DD-MM-YYYY LT",
    minDate: new Date(),
  });
  $("#btn_term").click(function () {
    var rotate = parseInt(
      $("#icon_term").attr("style").split("rotate(")[1].split("deg)")[0]
    );
    if (rotate === 0) {
      document.getElementById("icon_term").style =
        "transform: rotate(180deg); width: 25px; height: 25px";
    } else {
      document.getElementById("icon_term").style =
        "transform: rotate(0deg); width: 25px; height: 25px";
    }
  });
  $("#btn_condition").click(function () {
    var rotate = parseInt(
      $("#icon_condition").attr("style").split("rotate(")[1].split("deg)")[0]
    );
    if (rotate === 0) {
      document.getElementById("icon_condition").style =
        "transform: rotate(180deg); width: 25px; height: 25px";
    } else {
      document.getElementById("icon_condition").style =
        "transform: rotate(0deg); width: 25px; height: 25px";
    }
  });
  $("#btn_choose_criteria").click(function () {
    var display = $("#box_criteria").css(["display"]);
    if (display.display === "none") {
      document.getElementById("box_criteria").style = "display: block";
    } else {
      document.getElementById("box_criteria").style = "display: none";
    }
  });
  $("#btn_done").click(function (e) {
    e.stopPropagation();
    var display = $("#box_criteria").css(["display"]);
    if (display.display === "none") {
      document.getElementById("box_criteria").style = "display: block";
    } else {
      document.getElementById("box_criteria").style = "display: none";
    }
  });
  $("#open_popover").click(function (e) {
    e.stopPropagation();
    document.getElementById("box_popover_id").style = "display: block";
  });
  $("#box_popover_id").click(function (e) {
    e.stopPropagation();
  });
  $("#buttonWaiting").click(function () {
    var rotate = parseInt(
      $("#iconWaiting").attr("style").split("rotate(")[1].split("deg)")[0]
    );
    if (rotate === 0) {
      $("#collapseWaiting").removeClass("show");
      document.getElementById("iconWaiting").style =
        "color: #888888; cursor: pointer; transform: rotate(180deg);";
    } else {
      document.getElementById("iconWaiting").style =
        "color: #888888; cursor: pointer; transform: rotate(0deg);";
    }
  });
  $("#buttonAccepted").click(function () {
    var rotate = parseInt(
      $("#iconAccepted").attr("style").split("rotate(")[1].split("deg)")[0]
    );
    if (rotate === 0) {
      $("#collapseAccepted").removeClass("show");
      document.getElementById("iconAccepted").style =
        "color: #888888; cursor: pointer; transform: rotate(180deg);";
    } else {
      document.getElementById("iconAccepted").style =
        "color: #888888; cursor: pointer; transform: rotate(0deg);";
    }
  });
  $("#buttonCancelled").click(function () {
    var rotate = parseInt(
      $("#iconCancelled").attr("style").split("rotate(")[1].split("deg)")[0]
    );
    if (rotate === 0) {
      $("#collapseCancelled").removeClass("show");
      document.getElementById("iconCancelled").style =
        "color: #888888; cursor: pointer; transform: rotate(180deg);";
    } else {
      document.getElementById("iconCancelled").style =
        "color: #888888; cursor: pointer; transform: rotate(0deg);";
    }
  });
  $("#buttonDone").click(function () {
    var rotate = parseInt(
      $("#iconDone").attr("style").split("rotate(")[1].split("deg)")[0]
    );
    if (rotate === 0) {
      $("#collapseDone").removeClass("show");
      document.getElementById("iconDone").style =
        "color: #888888; cursor: pointer; transform: rotate(180deg);";
    } else {
      document.getElementById("iconDone").style =
        "color: #888888; cursor: pointer; transform: rotate(0deg);";
    }
  });
});
$(document).ready(function () {
  $(".js-select2-multi").select2();
  $("#toggle-state-switch").change(function () {
    console.log("first");
  });
});
